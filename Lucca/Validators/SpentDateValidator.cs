﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Lucca.Validators
{
    public class SpentDateValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            DateTime valueDate = (DateTime)value;

            DateTime now = DateTime.Now;
            if (valueDate > now || valueDate < now.AddMonths(-3))
            {
                return new ValidationResult("Date shouldn't be in the future and mustn't be older than 3 months");
            }
            return ValidationResult.Success;
        }
    }
}
