﻿using Lucca.Daos;
using Lucca.Models;
using System;
using System.Linq;

namespace Lucca.Validators
{
    public class SpentValidator
    {
        private IUserDao _userDao;
        private ISpentDao _spentDao;

        public SpentValidator(IUserDao userDao, ISpentDao spentDao)
        {
            _userDao = userDao;
            _spentDao = spentDao;
        }
        public Boolean IsValid(Spent spent)
        {

            return IsValidCurrencyForUser(spent) && IsNotSameSpent(spent);
        }

        public Boolean IsValidCurrencyForUser(Spent spent)
        {
            User user = _userDao.FindById(spent.UserId);
            return user != null && user.Currency.Equals(spent.Currency);
        }

        public Boolean IsNotSameSpent(Spent spent)
        {
            var result = _spentDao.GetAll().ToList().Exists(s => s.Equals(spent));
            return !result;
        }
    }
}
