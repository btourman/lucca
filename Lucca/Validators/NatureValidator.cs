﻿using Lucca.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace Lucca.Validators
{
    public class NatureValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string[] natures = Nature.GetAll();
            if (value is string && Array.Exists(natures, nature => nature.Equals((string)value)))
            {
                return ValidationResult.Success;
            }

            return new ValidationResult(String.Format($"Nature should be {string.Join(", ", natures)}"));
        }
    }
}
