﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Lucca.Migrations
{
    public partial class InitMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Lastname = table.Column<string>(nullable: false),
                    Firstname = table.Column<string>(nullable: false),
                    Currency = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Spents",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Nature = table.Column<string>(nullable: false),
                    Amount = table.Column<long>(nullable: false),
                    Currency = table.Column<string>(nullable: false),
                    Comment = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Spents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Spents_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Currency", "Firstname", "Lastname" },
                values: new object[] { new Guid("4a92f795-8c6c-4ef9-bc9b-998f008e3467"), "$", "Anthony", "Stark" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Currency", "Firstname", "Lastname" },
                values: new object[] { new Guid("99ba0696-14d4-445e-892e-87dac02b9225"), "₽", "Natasha", "Romanova" });

            migrationBuilder.CreateIndex(
                name: "IX_Spents_UserId",
                table: "Spents",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Spents");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
