﻿using AutoMapper;
using Lucca.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lucca.Dtos
{
    public class SpentMapper
    {
        private static MapperConfiguration config = new MapperConfiguration(cfg => cfg.CreateMap<Spent, SpentDto>()
        .ForMember(dto => dto.User, map => map.MapFrom(source => $"{source.User.Firstname} {source.User.Lastname}")));

        public static SpentDto Map(Spent spent)
        {
            return config.CreateMapper().Map<SpentDto>(spent);
        }

        public static IEnumerable<SpentDto> Map(IEnumerable<Spent> spents)
        {
            return spents.Select(s => config.CreateMapper().Map<SpentDto>(s));
        }
    }
}
