﻿using System;

namespace Lucca
{
    public class SpentDto
    {
        public Guid Id { get; set; }
        public string User { set; get; }

        public DateTime Date { get; set; }

        public string Nature { get; set; }

        public long Amount { get; set; }

        public string Currency { get; set; }

        public string Comment { get; set; }
    }
}
