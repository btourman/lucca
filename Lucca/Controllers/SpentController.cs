﻿using Lucca.Daos;
using Lucca.Dtos;
using Lucca.Models;
using Lucca.Utils;
using Lucca.Validators;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Mime;

namespace Lucca.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SpentController : ControllerBase
    {
        private ISpentDao _spentDao;
        private IUserDao _userDao;


        public SpentController(ISpentDao spentDao, IUserDao userDao)
        {
            _spentDao = spentDao;
            _userDao = userDao;
        }

        [HttpGet]
        public IEnumerable<SpentDto> Get(string user, [FromQuery] string[] sorts)
        {
            IEnumerable<Spent> spents;
            if (user != null)
            {
                spents = _spentDao.GetByUser(user);
            }
            else
            {
                spents = _spentDao.GetAll();
            }

            List<Tuple<string, string>> sortItems = new List<Tuple<string, string>>();

            foreach(string sort in sorts)
            {
                string[] splitSort = sort.Split(":");
                sortItems.Add(Tuple.Create(splitSort[0], splitSort.Length > 1 ? splitSort[1] : ""));
            }

            return SpentMapper.Map(spents.AsQueryable().OrderByPropertyName(sortItems));
        }

        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        public ActionResult<SpentDto> Post([FromBody] Spent spent)
        {
            if (new SpentValidator(_userDao, _spentDao).IsValid(spent))
            {
                Spent newSpent = _spentDao.Insert(spent);
                return Created(new Uri($"{Request.Host}{Request.Path}/{newSpent.Id}"), SpentMapper.Map(newSpent));
            }
            return BadRequest("The spent already exists or the currency is invalid inrelation to the user");
        }
    }
}
