﻿using Lucca.Models;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Lucca.Daos
{
    public class SpentDao : ISpentDao
    {
        private DatabaseContext _context;

        public SpentDao(DatabaseContext context)
        {
            _context = context;
        }

        public IEnumerable<Spent> GetAll()
        {
            return _context.Spents.Include(s => s.User);
        }

        public IEnumerable<Spent> GetByUser(string userId)
        {
            return _context.Spents.Include(s => s.User).ToList().Where(s => s.UserId.ToString().Equals(userId));
        }

        public Spent Insert(Spent spent)
        {
            Spent newSpent = _context.Add(spent).Entity;
            _context.SaveChanges();

            return newSpent;
        }

    }
}
