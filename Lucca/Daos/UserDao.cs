﻿using Lucca.Models;
using System;

namespace Lucca.Daos
{
    public class UserDao : IUserDao
    {
        private DatabaseContext _context;

        public UserDao(DatabaseContext context)
        {
            _context = context;
        }

        public User FindById(Guid uuid)
        {
            return _context.Users.Find(uuid);
        }
    }
}
