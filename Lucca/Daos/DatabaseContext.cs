﻿using Lucca.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace Lucca.Daos
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext()
        { }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        { }

        public DbSet<Spent> Spents { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite("Data Source=lucca.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Lastname).IsRequired();
                entity.Property(e => e.Firstname).IsRequired();
                entity.Property(e => e.Currency).IsRequired();
            });

            #region UserSeed
            modelBuilder.Entity<User>().HasData(
                new User { Id = Guid.NewGuid(), Lastname = "Stark", Firstname = "Anthony", Currency = "$" },
                new User { Id = Guid.NewGuid(), Lastname = "Romanova", Firstname = "Natasha", Currency = "₽" }
                );

            modelBuilder.Entity<Spent>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Comment).IsRequired();
                entity.HasOne(spent => spent.User)
                    .WithMany(user => user.Spents)
                    .HasForeignKey(spent => spent.UserId);
            });
            #endregion

        }
    }

}
