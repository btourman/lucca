﻿using Lucca.Models;
using System.Collections.Generic;

namespace Lucca.Daos
{
    public interface ISpentDao
    {
        IEnumerable<Spent> GetAll();

        IEnumerable<Spent> GetByUser(string userId);

        Spent Insert(Spent spent);
    }
}
