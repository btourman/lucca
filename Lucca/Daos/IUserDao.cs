﻿using Lucca.Models;
using System;

namespace Lucca.Daos
{
    public interface IUserDao
    {
        User FindById(Guid uuid);
    }
}
