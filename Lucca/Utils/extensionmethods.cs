﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Lucca.Utils
{
    public static class extensionmethods
    {
        public static IQueryable<T> OrderByPropertyName<T>(this IQueryable<T> q, IEnumerable<Tuple<string,string>> sortItems)
        {
            foreach (Tuple<string, string> sortItem in sortItems)
            {
                Type type = typeof(T);
                ParameterExpression param = Expression.Parameter(type, "p");
                if(type.GetProperty(StringUtil.Capitalize(sortItem.Item1)) == null)
                {
                    continue;
                }
                MemberExpression prop = Expression.Property(param, sortItem.Item1);
                LambdaExpression exp = Expression.Lambda(prop, param);
                string method = "OrderBy";
                if ("desc".Equals(sortItem.Item2))
                {
                    method = "OrderByDescending";
                }
                Type[] types = new Type[] { q.ElementType, exp.Body.Type };
                MethodCallExpression rs = Expression.Call(typeof(Queryable), method, types, q.Expression, exp);
                q = q.Provider.CreateQuery<T>(rs);
            }
            return q;
        }
    }
}
