﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lucca.Utils
{
    public class StringUtil
    {
        public static String Capitalize(string text)
        {
            return char.ToUpper(text[0]) + text.Substring(1);
        }
    }
}
