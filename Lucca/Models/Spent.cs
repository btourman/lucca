﻿using Lucca.Validators;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Lucca.Models
{
    public class Spent
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public Guid Id { get; set; }
        [Required]
        public Guid UserId { get; set; }
        [JsonIgnoreAttribute]
        public User User { set; get; }
        [Required]
        [SpentDateValidator]
        public DateTime Date { get; set; }

        [Required]
        [NatureValidator]
        public string Nature { get; set; }

        [Required]
        [Range(1, Int64.MaxValue)]
        [Description("A positive integer representing the amount in the smallest currency unit")]
        public long Amount { get; set; }
        [Required]
        public string Currency { get; set; }
        [Required]
        public string Comment { get; set; }

        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Spent spent = (Spent)obj;
                return spent.Amount == Amount && (int)Math.Round(spent.Date.Subtract(Date).TotalMinutes) == 0 && spent.UserId == UserId;
            }
        }

        public override int GetHashCode()
        {
            return (Convert.ToInt32(Amount) << 2) ^ Convert.ToInt32(Date.Ticks) * Convert.ToInt32(UserId);
        }
    }
}
