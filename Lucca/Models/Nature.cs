﻿namespace Lucca.Models
{
    public class Nature
    {
        public const string Restaurant = "Restaurant";
        public const string Hotel = "Hotel";
        public const string Misc = "Misc";

        public static string[] GetAll()
        {
            return new string[] { Restaurant, Hotel, Misc };
        }
    }
}
