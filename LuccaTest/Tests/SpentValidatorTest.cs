﻿using Lucca.Daos;
using Lucca.Models;
using Lucca.Validators;
using Moq;
using System;
using Xunit;

namespace LuccaTest.Tests
{
    public class SpentValidatorTest
    {
        [Fact]
        public void CurrencyOK()
        {
            Spent spent = new Spent() { Amount = 15, Currency = "$", Comment = "test", Date = DateTime.Now, Nature = "Restaurant" };

            var userDao = new Mock<IUserDao>();
            var spentDao = new Mock<ISpentDao>().Object;

            userDao.Setup(_ => _.FindById(It.IsAny<Guid>())).Returns(new User() { Currency = "$" });

            Assert.True(new SpentValidator(userDao.Object, spentDao).IsValidCurrencyForUser(spent));
        }

        [Fact]
        public void CurrencyKO()
        {
            var userDao = new Mock<IUserDao>();
            var spentDao = new Mock<ISpentDao>().Object;

            userDao.Setup(_ => _.FindById(It.IsAny<Guid>())).Returns(new User() { Currency = "$" });

            Spent spent = new Spent() { Amount = 15, Currency = "₽", Comment = "test", Date = DateTime.Now, Nature = "Test" };

            Assert.False(new SpentValidator(userDao.Object, spentDao).IsValidCurrencyForUser(spent));
        }

        [Fact]
        public void IsSameSpent()
        {
            var userDao = new Mock<IUserDao>();
            var spentDao = new Mock<ISpentDao>();

            Spent spent1 = new Spent() { Amount = 15, Currency = "$", Comment = "test", Date = DateTime.Now, Nature = "Test" };
            Spent spent2 = new Spent() { Amount = 15, Currency = "$", Comment = "test", Date = DateTime.Now.AddDays(-2), Nature = "Test" };

            Spent postToSpent = new Spent() { Amount = 15, Currency = "$", Comment = "test", Date = DateTime.Now, Nature = "Test" };

            spentDao.Setup(_ => _.GetAll()).Returns(new Spent[] { spent1, spent2 });


            Assert.False(new SpentValidator(userDao.Object, spentDao.Object).IsNotSameSpent(postToSpent));
        }


        [Fact]
        public void IsNotSameSpentDifferentAmount()
        {
            var userDao = new Mock<IUserDao>();
            var spentDao = new Mock<ISpentDao>();

            Spent spent1 = new Spent() { Amount = 15, Currency = "$", Comment = "test", Date = DateTime.Now, Nature = "Test" };
            Spent spent2 = new Spent() { Amount = 15, Currency = "$", Comment = "test", Date = DateTime.Now.AddDays(-2), Nature = "Test" };

            spentDao.Setup(_ => _.GetAll()).Returns(new Spent[] { spent1, spent2 });

            Spent postToSpent = new Spent() { Amount = 20, Currency = "$", Comment = "test", Date = DateTime.Now, Nature = "Test" };

            Assert.True(new SpentValidator(userDao.Object, spentDao.Object).IsNotSameSpent(postToSpent));
        }

        [Fact]
        public void IsNotSameSpentDifferentDate()
        {
            var userDao = new Mock<IUserDao>();
            var spentDao = new Mock<ISpentDao>();

            Spent spent1 = new Spent() { Amount = 15, Currency = "$", Comment = "test", Date = DateTime.Now, Nature = "Test" };
            Spent spent2 = new Spent() { Amount = 15, Currency = "$", Comment = "test", Date = DateTime.Now.AddDays(-2), Nature = "Test" };

            spentDao.Setup(_ => _.GetAll()).Returns(new Spent[] { spent1, spent2 });

            Spent postToSpent = new Spent() { Amount = 15, Currency = "$", Comment = "test", Date = DateTime.Now.AddDays(-1), Nature = "Test" };

            Assert.True(new SpentValidator(userDao.Object, spentDao.Object).IsNotSameSpent(postToSpent));
        }
    }
}
