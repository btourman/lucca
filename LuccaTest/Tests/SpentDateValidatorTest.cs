﻿using Lucca.Models;
using Lucca.Validators;
using System;
using Xunit;

namespace LuccaTest.Tests
{
    public class SpentDateValidatorTest
    {
        [Fact]
        public void DateOK()
        {
            Spent spent = new Spent() { Amount = 15, Currency = "$", Comment = "test", Date = DateTime.Now, Nature = "Restaurant" };

            Assert.True(new SpentDateValidator().IsValid(spent.Date));
        }

        [Fact]
        public void DateInFutureKO()
        {
            Spent spent = new Spent() { Amount = 15, Currency = "$", Comment = "test", Date = DateTime.Now.AddDays(1), Nature = "Test" };

            Assert.False(new SpentDateValidator().IsValid(spent.Date));
        }

        [Fact]
        public void DateOlderThan3MonthsKO()
        {
            Spent spent = new Spent() { Amount = 15, Currency = "$", Comment = "test", Date = DateTime.Now.AddMonths(-4), Nature = "Test" };

            Assert.False(new SpentDateValidator().IsValid(spent.Date));
        }
    }
}
