﻿using Lucca.Models;
using Lucca.Validators;
using System;
using Xunit;

namespace Lucca.Tests
{
    public class NatureValidatorTest
    {
        [Fact]
        public void NatureOK()
        {
            Spent spent = new Spent() { Amount = 15, Currency = "$", Comment = "test", Date = DateTime.Now, Nature = "Restaurant" };

            Assert.True(new NatureValidator().IsValid(spent.Nature));
        }

        [Fact]
        public void NatureKO()
        {
            Spent spent = new Spent() { Amount = 15, Currency = "$", Comment = "test", Date = DateTime.Now, Nature = "Test" };

            Assert.False(new NatureValidator().IsValid(spent.Nature));
        }
    }
}
